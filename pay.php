<?php session_start(); ?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Make a payment</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <a href="main.php"><img src="logo.png" alt="Logo" style="width:325px; height:58px;"></a>
                    </div>
                </div>     
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class='panel panel-default text-right'>
                        Welcome, <b><?php echo $_SESSION["uname"]; ?></b>.
                        <a href="logout.php" class="btn btn-primary btn-xs">Log Out</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-9">
                    <div class="col-md-12">
                        <div class="row">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4><b>Στοιχεία πληρωμής</b></h4>
                                </div>
                                <?php
                                if ('POST' === $_SERVER['REQUEST_METHOD']) {
                                    $step = filter_input(INPUT_POST, 'step');
                                    include 'functions.php';
                                    $conn = mysqlConnect();
                                    $uname = $_SESSION["uname"];
                                    $sql = "SELECT * FROM users WHERE username='$uname'";
                                    $result = mysqli_query($conn, $sql);
                                    $resultArray = mysqli_fetch_assoc($result);
                                    $cardNo1 = $resultArray['cardNo1'];
                                    $cardNo2 = $resultArray['cardNo2'];
                                    $cardBalance1 = $resultArray['cardBalance1'];
                                    $cardBalance2 = $resultArray['cardBalance2'];
                                    $accountBalance1 = $resultArray['accountBalance1'];
                                    $accountBalance2 = $resultArray['accountBalance2'];
                                    $type = filter_input(INPUT_POST, 'type');

                                    if ($step == 'step1') {
                                        if ($type == 'creditCard') {
                                            $account = filter_input(INPUT_POST, 'account');
                                            $accountNo = $resultArray[$account];
                                            echo "Έχετε επιλέξει τον λογαριασμό <b>'", $accountNo, "'</b> για πληρωμή πιστοτικής κάρτας";
                                            ?>
                                            <form method="post" class="form" action="pay.php">
                                                <div class="panel-body">
                                                    <label for="card">Επιλέξτε κάρτα για αποπληρωμή</label>
                                                    <select class="form-control" name="card" required>
                                                        <option value="card1"><?php echo $cardNo1; ?></option>
                                                        <option value="card2"><?php echo $cardNo2; ?></option>
                                                    </select>
                                                    <label for="card">Εισάγετε ποσό σε €: </label>
                                                    <input class="form-control" type="number" name="amount"/>

                                                    <input type="hidden" name ="account" value="<?php echo $account; ?>"/>
                                                    <input type="hidden" name ="type" value="creditCard"/>
                                                    <input type="hidden" name="step" value="step2"/>
                                                </div>

                                                <div class="panel-footer text-right">
                                                    <button type="submit" class="btn btn-primary">Ολοκλήρωση πληρωμής</button>
                                                </div>
                                            </form>
                                            <?php
                                        } else if ($type == 'bill') {
                                            $account = filter_input(INPUT_POST, 'account');
                                            $accountNo = $resultArray[$account];
                                            echo "Έχετε επιλέξει τον λογαριασμό <b>'", $accountNo, "'</b> για πληρωμή λογαριασμού";
                                            ?>
                                            <form method="post" class="form" action="pay.php">
                                                <div class="panel-body">
                                                    <label for="service">Εισάγετε όνομα υπηρεσίας για αποπληρωμή λογαριασμού: </label>
                                                    <input class="form-control" type="text" name="service"/>
                                                    <label for="serviceCode">Εισάγετε κωδικό λογαριασμού: </label>
                                                    <input class="form-control" type="text" name="serviceCode"/>
                                                    <label for="amount">Εισάγετε ποσό σε €: </label>
                                                    <input class="form-control" type="number" name="amount"/>

                                                    <input type="hidden" name="step" value="step2"/>
                                                    <input type="hidden" name ="account" value="<?php echo $account; ?>"/>
                                                    <input type="hidden" name ="type" value="bill"/>
                                                </div>

                                                <div class="panel-footer text-right">
                                                    <button type="submit" class="btn btn-primary">Ολοκλήρωση πληρωμής</button>
                                                </div>
                                            </form>
                                            <?php
                                        }
                                    } else if ($step == 'step2') {
                                        $account = filter_input(INPUT_POST, 'account');
                                        if ($type == 'creditCard') {
                                            $card = filter_input(INPUT_POST, 'card');
                                            $amount = filter_input(INPUT_POST, 'amount');
                                            if ($card == 'card1') {
                                                $sql = "UPDATE users SET cardBalance1=$cardBalance1-$amount WHERE username='$uname'";
                                                if ($account == 'accountNo1') {
                                                    $sql2 = "UPDATE users SET accountBalance1=$accountBalance1-$amount WHERE username='$uname'";
                                                    if (mysqli_query($conn, $sql) && mysqli_query($conn, $sql2)) {
                                                        echo "Επιτυχής διεκπεραίωση συναλλαγής!";
                                                    } else {
                                                        echo "Η συναλλαγή απέτυχε.";
                                                    }
                                                } else if ($account == 'accountNo2') {
                                                    $sql2 = "UPDATE users SET accountBalance2=$accountBalance2-$amount WHERE username='$uname'";
                                                    if (mysqli_query($conn, $sql) && mysqli_query($conn, $sql2)) {
                                                        echo "Επιτυχής διεκπεραίωση συναλλαγής!";
                                                    } else {
                                                        echo "Η συναλλαγή απέτυχε.";
                                                    }
                                                }
                                            } else if ($card == 'card2') {
                                                $sql = "UPDATE users SET cardBalance2=$cardBalance2-$amount WHERE username='$uname'";
                                                if ($account == 'accountNo1') {
                                                    $sql2 = "UPDATE users SET accountBalance1=$accountBalance1-$amount WHERE username='$uname'";
                                                    if (mysqli_query($conn, $sql) && mysqli_query($conn, $sql2)) {
                                                        echo "Επιτυχής διεκπεραίωση συναλλαγής!";
                                                    } else {
                                                        echo "Η συναλλαγή απέτυχε.";
                                                    }
                                                } else if ($account == 'accountNo2') {
                                                    $sql2 = "UPDATE users SET accountBalance2=$accountBalance2-$amount WHERE username='$uname'";
                                                    if (mysqli_query($conn, $sql) && mysqli_query($conn, $sql2)) {
                                                        echo "Επιτυχής διεκπεραίωση συναλλαγής!";
                                                    } else {
                                                        echo "Η συναλλαγή απέτυχε.";
                                                    }
                                                }
                                            }
                                        } else if ($type == 'bill') {
                                            $account = filter_input(INPUT_POST, 'account');
                                            $accountNo = $resultArray[$account];
                                            $amount = filter_input(INPUT_POST, 'amount');
                                            if ($account == 'accountNo1') {
                                                $sql = "UPDATE users SET accountBalance1=$accountBalance1-$amount WHERE username='$uname'";
                                                if (mysqli_query($conn, $sql)) {
                                                    echo "Επιτυχής διεκπεραίωση συναλλαγής!";
                                                } else {
                                                    echo "Η συναλλαγή απέτυχε.";
                                                }
                                            } else if ($account == 'accountNo2') {
                                                $sql = "UPDATE users SET accountBalance2=$accountBalance2-$amount WHERE username='$uname'";
                                                if (mysqli_query($conn, $sql)) {
                                                    echo "Επιτυχής διεκπεραίωση συναλλαγής!";
                                                } else {
                                                    echo "Η συναλλαγή απέτυχε.";
                                                }
                                            }
                                        }
                                    }
                                } else { //if form method isn't post, show the initial form
                                    include 'functions.php';
                                    $conn = mysqlConnect();
                                    $uname = $_SESSION["uname"];
                                    $sql = "SELECT * FROM users WHERE username='$uname'";
                                    $result = mysqli_query($conn, $sql);
                                    $resultArray = mysqli_fetch_assoc($result);
                                    $accountNo1 = $resultArray['accountNo1'];
                                    $accountNo2 = $resultArray['accountNo2'];
                                    ?>

                                    <form method="post" class="form" action="pay.php">
                                        <div class="panel-body">
                                            <label for="account">Επιλέξτε λογαριασμό χρέωσης</label>
                                            <select class="form-control" name="account" required>
                                                <option value="accountNo1"><?php echo $accountNo1; ?></option>
                                                <option value="accountNo2"><?php echo $accountNo2; ?></option>
                                            </select>

                                            <label for="type">Επιλέξτε είδος πληρωμής</label>
                                            <select class="form-control" name="type" required>
                                                <option value="creditCard">Αποπληρωμή πιστωτικής κάρτας</option>
                                                <option value="bill">Αποπληρωμή λογαριασμού</option>
                                            </select>

                                            <input type="hidden" name="step" value="step1"/>
                                        </div>
                                        <div class="panel-footer text-right">
                                            <button type="submit" class="btn btn-primary">Επόμενο ></button>
                                        </div>
                                    </form>
                                    <?php
                                }
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="btn-group-vertical btn-block" role="group" aria-label="...">
                        <a href="main.php" class="btn btn-primary">Επιστροφή στην αρχική σελίδα</a>
                    </div>
                </div>
            </div>

        </div>
    </body>
</html>
