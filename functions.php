<?php

function mysqlConnect() {
    $servername = "localhost";
    $dbUsername = "root";
    $dbPassword = "";
    $dbName = "php";

// Create connection with DB
    $conn = new mysqli($servername, $dbUsername, $dbPassword, $dbName);

// Check connection
    if ($conn->connect_error) {
        die("Connection to database failed: " . $conn->connect_error);
    }
    return $conn;
}
