<?php session_start(); ?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Add Account</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>
    <body>



        <div class="container">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <a href="main.php"><img src="logo.png" alt="Logo" style="width:325px; height:58px;"></a>
                    </div>
                </div>     
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class='panel panel-default text-right'>
                        Welcome, <b><?php echo $_SESSION["uname"]; ?></b>.
                        <a href="logout.php" class="btn btn-primary btn-xs">Log Out</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="text-center">
                        <?php
                        if ('POST' === $_SERVER['REQUEST_METHOD']) {

                            include 'functions.php';
                            $conn = mysqlConnect();

                            $accountNo = filter_input(INPUT_POST, 'accountNo');
                            $uname = $_SESSION["uname"];

                            $sql = "SELECT * FROM users WHERE username='$uname'";
                            $result = mysqli_query($conn, $sql);
                            $resultArray = mysqli_fetch_assoc($result);
                            $uId = $resultArray['U_id'];
                            $accountNo1 = $resultArray['accountNo1'];
                            $accountNo2 = $resultArray['accountNo2'];

                            if ($accountNo1 == NULL && $accountNo2 == NULL) {
                                $sql2 = "UPDATE users "
                                        . "SET accountNo1='$accountNo', accountBalance1=1000 "
                                        . "WHERE U_id=$uId";

                                if (mysqli_query($conn, $sql2)) {
                                    header("Location: main.php");
                                } else {
                                    echo "Error. <a href='main.php'>Go back</a>.";
                                }
                            } else if ($accountNo1 != NULL && $accountNo2 == NULL) {
                                $sql2 = "UPDATE users "
                                        . "SET accountNo2='$accountNo', accountBalance2=1000 "
                                        . "WHERE U_id=$uId";

                                if (mysqli_query($conn, $sql2)) {
                                    header("Location: main.php");
                                } else {
                                    echo "Something went wrong. <a href='addAccount.php'>Try again</a>.";
                                }
                            } else if ($accountNo1 != NULL && $accountNo2 != NULL) {
                                echo "<b>You can't add any more accounts. <a href='main.php'>Go back</a>.</b>";
                            }
                        } else {
                            ?>
                            <form method="post" class="form form-inline" action="addAccount.php">
                                <div class="form-group">
                                    <input type="text" name="accountNo" class="form-control" placeholder="Account Number" pattern="^GR[0-9]{25}" title="GR followed by 25 numbers" required autofocus>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-md btn-primary" type="submit">Submit</button>
                                </div>
                                <div class="form-group">
                                    <a href="main.php" class="btn btn-md btn-danger">Cancel</a>
                                </div>
                            </form>
                            <?php
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
