<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>
    <body id="reg-body">

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 panel panel-default">
                    <img src="logo.png" alt="Logo" style="width:325px; height:58px;">
                </div>            
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4 panel panel-default">
                    <h1 class="text-center login-title">Register</h1>
                    <div class="account-wall">
                        <form method="post" class="form-signin" action="registerController.php"><br>

                            <input type="text" name="name" class="form-control" placeholder="Name" required autofocus>
                            <input type="text" name="surname" class="form-control" placeholder="Surname" required>
                            <input type="text" name="uname" class="form-control" placeholder="Username" required >
                            <input type="password" name="pass" class="form-control" placeholder="Password" required>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>

                        </form>
                    </div>
                    <a href="index.php" class="text-center new-account">Login</a>
                </div>
            </div>
        </div>
    </body>
</html>
