<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 panel panel-default">
                    <img src="logo.png" alt="Logo" style="width:325px; height:58px;">
                </div>            
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4 panel panel-default">
                    <h1 class="text-center login-title">Login</h1>
                    <div class="account-wall">
                        <form method="post" class="form-signin" action="loginController.php">

                            <input type="text" name="uname" class="form-control" placeholder="Username" required autofocus>
                            <input type="password" name="pass" class="form-control" placeholder="Password" required>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>

                        </form>
                    </div>
                    <a href="register.php" class="text-center new-account">Create an account </a>
                </div>
            </div>
        </div>
    </body>
</html>
