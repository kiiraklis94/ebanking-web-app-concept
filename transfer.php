<?php session_start(); ?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Transfer money</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <a href="main.php"><img src="logo.png" alt="Logo" style="width:325px; height:58px;"></a>
                    </div>
                </div>     
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class='panel panel-default text-right'>
                        Welcome, <b><?php echo $_SESSION["uname"]; ?></b>.
                        <a href="logout.php" class="btn btn-primary btn-xs">Log Out</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-9">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4><b>Στοιχεία μεταφοράς</b></h4>
                                </div>
                                <?php
                                if ('POST' === $_SERVER['REQUEST_METHOD']) {
                                    include 'functions.php';
                                    $conn = mysqlConnect();
                                    $uname = $_SESSION["uname"];
                                    $transferFrom = filter_input(INPUT_POST, 'transferFrom');
                                    $transferTo = filter_input(INPUT_POST, 'transferTo');
                                    $amount = filter_input(INPUT_POST, 'amount');

                                    $sql = "SELECT * FROM users WHERE accountNo1='$transferTo' OR accountNo2='$transferTo'";
                                    $result = mysqli_query($conn, $sql);
                                    $resultArray = mysqli_fetch_assoc($result);
                                    $userTo = $resultArray['username'];
                                    $userTo_AN1 = $resultArray['accountNo1'];
                                    $userTo_AN2 = $resultArray['accountNo2'];
                                    
                                    if ($transferFrom == 'account1') {
                                        $sql = "UPDATE users SET accountBalance1=accountBalance1-$amount WHERE username='$uname'";
                                        if (mysqli_query($conn, $sql)) {
                                            echo "Η μεταφορά ολοκληρώθηκε με επιτυχία.<br>";
                                        } else {
                                            echo "Η μεταφορά απέτυχε.<br>";
                                        }
                                    } else if ($transferFrom == 'account2') {
                                        $sql = "UPDATE users SET accountBalance2=accountBalance2-$amount WHERE username='$uname'";
                                        if (mysqli_query($conn, $sql)) {
                                            echo "Η μεταφορά ολοκληρώθηκε με επιτυχία.<br>";
                                        } else {
                                            echo "Η μεταφορά απέτυχε.<br>";
                                        }
                                    }
                                    if ($transferTo == $userTo_AN1) {
                                        $sql = "UPDATE users SET accountBalance1=accountBalance1+$amount WHERE username='$userTo'";
                                        if (mysqli_query($conn, $sql)) {
                                            echo "Η μεταφορά ολοκληρώθηκε με επιτυχία.";
                                        } else {
                                            echo "Η μεταφορά απέτυχε.";
                                        }
                                    } else if ($transferTo == $userTo_AN2) {
                                        $sql = "UPDATE users SET accountBalance2=accountBalance2+$amount WHERE username='$userTo'";
                                        if (mysqli_query($conn, $sql)) {
                                            echo "Η μεταφορά ολοκληρώθηκε με επιτυχία.";
                                        } else {
                                            echo "Η μεταφορά απέτυχε.";
                                        }
                                    }
                                } else {
                                    include 'functions.php';
                                    $conn = mysqlConnect();
                                    $uname = $_SESSION["uname"];
                                    $sql = "SELECT * FROM users WHERE username='$uname'";
                                    $result = mysqli_query($conn, $sql);
                                    $resultArray = mysqli_fetch_assoc($result);
                                    $accountNo1 = $resultArray['accountNo1'];
                                    $accountNo2 = $resultArray['accountNo2'];
                                    ?>
                                    <form method="post" class="form" action="transfer.php">
                                        <div class="panel-body">

                                            <label for="transferFrom">Από</label>
                                            <select class="form-control" name="transferFrom" required>
                                                <option value="account1"><?php echo "Αccount 1 (", $accountNo1, ")"; ?></option>
                                                <option value="account2"><?php echo "Αccount 2 (", $accountNo2, ")"; ?></option>
                                            </select>

                                            <label for="transferTo">Προς</label>
                                            <input class="form-control" type="text" name="transferTo" required/>

                                            <label for="amount">Ποσό σε €</label>
                                            <input class="form-control" type="number" name="amount" required/>
                                        </div>
                                        <div class="panel-footer text-right">
                                            <button type="submit" class="btn btn-primary">Ολοκλήρωση μεταφοράς</button>
                                        </div>
                                    </form>
                                    <?php
                                }
                                ?>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="btn-group-vertical btn-block" role="group" aria-label="...">
                        <a href="main.php" class="btn btn-primary">Επιστροφή στην αρχική σελίδα</a>
                    </div>
                </div>

            </div>



        </div>
    </body>
</html>
