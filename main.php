<?php session_start(); ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Main Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<!--        <script   src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>-->
    </head>

    <body>

        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <a href="main.php"><img src="logo.png" alt="Logo" style="width:325px; height:58px;"></a>
                    </div>
                </div>     
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class='panel panel-default text-right'>
                        Welcome, <b><?php echo $_SESSION["uname"]; ?></b>.
                        <a href="logout.php" class="btn btn-primary btn-xs">Log Out</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-9">
                    <div class="col-md-12">
                        <div class="row">
                            <?php
                            $uname2 = $_SESSION["uname"];
                            include 'functions.php';
                            $conn = mysqlConnect();
                            $sql2 = "SELECT * FROM users WHERE username='$uname2'";
                            $result2 = mysqli_query($conn, $sql2);
                            $resultArray2 = mysqli_fetch_assoc($result2);
                            $name = $resultArray2['name'];
                            $surname = $resultArray2['surname'];
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php
                                    echo "<h3><b>", $name, " ", $surname, "</b></h3>";
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row"> <!-- Λογαριασμοί -->
                            <?php
//                            include 'functions.php';
//                            $conn = mysqlConnect();

                            $uname = $_SESSION["uname"];

                            $sql = "SELECT * FROM users WHERE username='$uname'";
                            $result = mysqli_query($conn, $sql);
                            $resultArray = mysqli_fetch_assoc($result);

                            $accountNo1 = $resultArray['accountNo1'];
                            $accountNo2 = $resultArray['accountNo2'];
                            $accountBalance1 = $resultArray['accountBalance1'];
                            $accountBalance2 = $resultArray['accountBalance2'];

                            $cardNo1 = $resultArray['cardNo1'];
                            $cardNo2 = $resultArray['cardNo2'];
                            $cardBalance1 = $resultArray['cardBalance1'];
                            $cardBalance2 = $resultArray['cardBalance2'];
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4><b>Λογαριασμοί</b></h4>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-hover table-bordered">
                                        <tr>
                                            <th>Αριθμός Λογαριασμού</th>
                                            <th>Υπόλοιπο</th>
                                        </tr>
                                        <tr>
                                            <td><?php echo "<b>", substr($accountNo1, 0, 2), "</b> ", substr($accountNo1, 2, 25); ?></td>
                                            <td><?php
                                                if ($accountBalance1 != NULL) {
                                                    echo $accountBalance1, " €";
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?php echo "<b>", substr($accountNo2, 0, 2), "</b> ", substr($accountNo2, 2, 25); ?></td>
                                            <td><?php
                                                if ($accountBalance2 != NULL) {
                                                    echo $accountBalance2, " €";
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="panel-footer text-right">
                                    <?php
                                    if ($accountNo1 != NULL && $accountNo2 != NULL) {
                                        ?>
                                        <a href="#" class="btn btn-primary disabled">Προσθήκη νέου λογαριασμού</a>
                                        <?php
                                    } else {
                                        ?>
                                        <a href="addAccount.php" class="btn btn-primary ">Προσθήκη νέου λογαριασμού</a>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row"> <!-- Κάρτες --> 
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4><b>Πιστωτικές Κάρτες</b></h4>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-hover table-bordered">
                                        <tr>
                                            <th>Αριθμός πιστωτικής κάρτας</th>
                                            <th>Χρέος</th>
                                        </tr>
                                        <tr>
                                            <td><?php echo substr($cardNo1, 0, 4), " ", substr($cardNo1, 4, 4), " ", substr($cardNo1, 8, 4), " ", substr($cardNo1, 12, 4); ?></td>
                                            <td>
                                                <?php
                                                if ($cardBalance1 != NULL) {
                                                    echo "-", $cardBalance1, " €";
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?php echo substr($cardNo2, 0, 4), " ", substr($cardNo2, 4, 4), " ", substr($cardNo2, 8, 4), " ", substr($cardNo2, 12, 4); ?></td>
                                            <td>
                                                <?php
                                                if ($cardBalance2 != NULL) {
                                                    echo "-", $cardBalance2, " €";
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="panel-footer text-right">
                                    <?php
                                    if ($cardNo1 != NULL && $cardNo2 != NULL) {
                                        ?>
                                        <a href="#" class="btn btn-primary disabled">Προσθήκη νέας κάρτας</a>
                                        <?php
                                    } else {
                                        ?>
                                        <a href="addCard.php" class="btn btn-primary">Προσθήκη νέας κάρτας</a>
                                        <?php
                                    }
                                    ?>
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="btn-group-vertical btn-block" role="group" aria-label="...">
                        <a href="pay.php" class="btn btn-default">Πληρωμή</a>
                        <a href="transfer.php" class="btn btn-default">Μεταφορά χρημάτων</a>
                    </div>
                </div>
            </div>

        </div>
    </body>
</html>
